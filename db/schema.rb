# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_27_155003) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "personas", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "string"
    t.string "password_digest"
    t.index ["name"], name: "index_personas_on_name"
    t.index ["string"], name: "index_personas_on_string"
  end

  create_table "positions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "track_id"
    t.float "speed"
    t.float "accuracy"
    t.datetime "time"
    t.string "provider"
    t.geography "coordinates", limit: {:srid=>4326, :type=>"geometry", :has_z=>true, :geographic=>true}
    t.index ["track_id"], name: "index_positions_on_track_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "persona_id"
    t.string "name"
    t.index ["persona_id"], name: "index_tracks_on_persona_id"
  end

end
