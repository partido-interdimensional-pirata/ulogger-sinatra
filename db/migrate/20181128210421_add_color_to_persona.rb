class AddColorToPersona < ActiveRecord::Migration[5.2]
  def up
    add_column :personas, :color, :string

    Persona.find_each do |persona|
      persona.random_color!
      persona.save
    end
  end

  def down
    remove_column :personas, :color
  end
end
