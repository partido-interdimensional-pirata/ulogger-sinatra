class CreatePersonas < ActiveRecord::Migration[5.2]
  def change
    create_table :personas do |t|
      t.timestamps
      t.string :name, :string, index: true, unique: true
      t.string :encrypted_password, :string
    end
  end
end
