class RenameTracksId < ActiveRecord::Migration[5.2]
  def change
    rename_column :positions, :tracks_id, :track_id
  end
end
