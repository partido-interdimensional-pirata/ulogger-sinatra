class CreateGrupos < ActiveRecord::Migration[5.2]
  def change
    create_table :grupos do |t|
      t.timestamps
      t.string :name, unique: true
    end

    add_belongs_to :tracks, :grupo, index: true
  end
end
