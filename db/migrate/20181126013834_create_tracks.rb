class CreateTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :tracks do |t|
      t.timestamps
      t.belongs_to :persona, index: true
      t.string :name
    end
  end
end
