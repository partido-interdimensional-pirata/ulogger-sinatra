# Posiciones territoriales
class Position < ActiveRecord::Base
  belongs_to :track

  default_scope -> { order('created_at desc') }

  # Convertir los valores de un hash en un punto WKT, devolviendo nulo
  # si no contiene todos los valores necesarios.
  #
  # Incluye altitud.
  def self.hash_to_wkt(hash)
    return nil unless %w[lon lat altitude].map { |k| hash.keys.include? k }.all?

    "POINT(#{hash['lon']} #{hash['lat']} #{hash['altitude']})"
  end
end
